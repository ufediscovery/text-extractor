# README #

### text-extractor ###

Most of the time extremely important information is contained in Office document formats and PDF.

It may be useful to convert these documents into plain text for the purpose of data processing.

There are individual tools that convert these document formats into plain text.

This project is an attempt combine all these tools into one single package.

Text-extractor converts popular document formats like doc,docx,pdf,rtf,xls and xlsx into utf-8 

encoded text on Windows platform.

#### Currently supports the following file types:

* ppt
* pptx
* doc
* docx
* rtf
* xls
* xlsx
* pdf

### text-extractor uses the following python packages:


* PDFMiner ( a python module ) to convert pdf document to text
* win32com ( a python module ) 
* pyDocx ( a python module ) : for converting Docx to plain text.
* pyth ( a python module ) : required for RTF to plain text conversion.
* xlrd ( a python module ) : required for xls/xlsx to plain text conversion.

### List of application required to run text-extractor: 

* MS Powerpoint
* Antiword ( A command line utility tool ) : required for doc to plain text conversion.

### Setup ###

#### Antiword setup instructions:

* Install antiword in the same path as that of your program
* The text-extract program adds the environment variable to the path

### Command-line

    Example:

                python TextExtractor.py -d abc.pdf -o "C:\test\results\" -c utf-8








### Contribution guidelines ###

* Make sure your fork is up to date

                      git remote add ufediscovery https://bitbucket.org/ufediscovery/text-extractor/
                      
                      git pull ufediscovery master



* Create a branch

                      git checkout -b BRANCH_NAME




* To add and commit changes use:

                      git add text-extractor/test

                      git commit -am "YOUR_MSG"



* To push changes use:

                      git push origin BRANCH_NAME



### Who do I talk to? ###

* Repo owner or admin