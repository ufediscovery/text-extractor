#!/usr/bin/env python
# -*- coding: UTF-8 -*-
"""TextExtractor

Description : Converts popular document formats like pdf, doc, docx, xls, xlsx,
ppt, pptx, rtf into plain text.


Created on: Tuesday, June 24 2014
Created by: Srinivas Balaji Ramesh

"""

__version__ = "0.1b" # beta version 
# $Source$

print __doc__ # prints script info.

import xlrd
import os
import win32com.client
import subprocess
import argparse
import sys

from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter
from pdfminer.converter import TextConverter
from pdfminer.layout import LAParams
from pdfminer.pdfpage import PDFPage
from cStringIO import StringIO
from docx import opendocx, getdocumenttext
from pyth.plugins.rtf15.reader import Rtf15Reader
from pyth.plugins.plaintext.writer import PlaintextWriter
from pptx import Presentation

antiword = 'antiword.exe'

class TextExtractor:
    '''    
    Use this class to convert popular document formats like doc, docx, pdf, rtf,
    xls and xlsx into plain text.
    '''

    def __init__(self):        
        '''
        Set the the PATH variable and create new environment variable HOME
        '''
        os.environ['PATH'] += ';' + os.path.join(os.getcwd(), 'antiword')


    def identify_and_convert(self, src_path, codec = 'utf-8'):
        '''
        Identifies and converts popular file types to text
        
        Arguments: 
            src_path - path to the file,
            codec - (Eg: utf-8, utf-16 etc)
        
        Returns: 
            unicode string 
            
            
        '''
        
        # TODO: use pyMagic to identify MIME type using the magic number  
        _, file_ext =  os.path.splitext(src_path.strip().lower())
        
        if file_ext == '.pdf':
            result = self.convert_pdf_to_txt(src_path, codec)
            
        elif file_ext == '.doc' or file_ext == '.docx':
            result = self.convert_word_doc_to_txt(src_path, codec, file_ext)
            
        elif file_ext == '.ppt' or file_ext == '.pptx':
            result = convert_ppt_doc_to_txt(src_path, codec, file_ext)
            
        elif file_ext == '.rtf':
            result = self.convert_rtf_doc_to_txt(src_path, codec)
            
        elif file_ext == '.xlsx' or file_ext == '.xls':
            result = self.convert_xls_doc_to_txt(src_path, codec)
            
        else:
            result = '-10231 ' + file_ext + ' : unsupported document format'

        return result.encode(codec,'ignore')

    
    def convert_pdf_to_txt(self, src_path, codec = 'utf-8'):
        '''   
        Converts PDF documents into text and stores them into the file mentioned
        in the destination path.
        
        Arguments: 
            src_path - path to the pdf file,
            codec - (Eg: utf-8, utf-16 etc)
        
        Returns: 
            unicode string       
        '''
        rsrcmgr = PDFResourceManager()
        retstr = StringIO()
        laparams = LAParams()
        device = TextConverter(rsrcmgr, retstr, codec=codec, laparams=laparams)
        with open(src_path, 'rb') as fp:
            interpreter = PDFPageInterpreter(rsrcmgr, device)
            password = ""
            maxpages = 0
            caching = True
            pagenos = set()        
            for page in PDFPage.get_pages(fp, pagenos, maxpages=maxpages,
                                        password=password, caching=caching,
                                        check_extractable=True):
                interpreter.process_page(page)
        pdf_txt = retstr.getvalue()
        pdf_txt = unicode(pdf_txt, codec,'ignore')
        retstr.close()
        
        return pdf_txt
    

    def convert_word_doc_to_txt(self, src_path, codec, file_ext):
        '''  
        Converts MS-Word documents into text and stores them into the file
        mentioned in the destination path.
        
        Arguments: 
            src_path - path to the doc/docx file,
            codec - (Eg: utf-8, utf-16 etc)
            file_ext - .doc or .docx
            
        Returns: 
            doc_text - unicode string

        Note: The folder containing antiword.exe should also be named antiword.
              The python file and the antiword folder should be in the same 
              path.
        '''
        if file_ext == '.doc':
            doc_text = ''
            try:
                doc_text = subprocess.check_output(antiword + ' "' + src_path 
                                                   + '"', shell=True)
            except: 
                print 'Unable find identify the %s file as Doc file' % src_path
        
        elif file_ext == '.docx':
            document = opendocx(src_path)
            paragraphTextList = getdocumenttext(document)
            newParaTextList = []
            doc_text = ''
            for text in paragraphTextList:
                newParaTextList.append(text.encode(codec))
            doc_text = '\n\n'.join(newParaTextList)
            doc_text = unicode(doc_text, codec, 'ignore')
        
        return doc_text
            

#   Note: Use "pip install xlrd" to install the required package for this functionality
    def convert_xls_doc_to_txt(self, src_path, codec):
        '''
        Converts MS-Excel documents both xls and xlsx documents into text and
        stores them into the file mentioned in the destination path.

        Arguments: 
            src_path - path to the xls/xlsx file,
            codec - (Eg: utf-8, utf-16 etc.
        
        Returns: 
            text_in_xls_list - unicode string
        '''
        wb = xlrd.open_workbook(src_path)
        text_in_xls = ''
        for i in range(0,wb.nsheets):
            sh = wb.sheet_by_index(i)
            for rownum in range(sh.nrows):
                for val in sh.row_values(rownum):
                    text_in_xls = str(val) + '\t' + text_in_xls
                text_in_xls = '\n' + text_in_xls  
        text_in_xls_list = unicode(text_in_xls, codec, 'ignore')
        return text_in_xls_list
            
                
    
  
    def convert_rtf_doc_to_txt(self, src_path, codec):
        '''  
        Converts RTF documents into text and stores them into the file
        mentioned in the destination path.
        
        Note: Needs pyth package. Use "pip install pyth" to install the package.
            
        Arguments: 
            src_path - path to the doc/doc file,
            codec - (Eg: utf-8, utf-16 etc. 
                             
        Returns: 
            txt_in_rtf_doc - unicode string
        '''
        doc = Rtf15Reader.read(open(src_path))
        txt_in_rtf_doc = PlaintextWriter.write(doc).getvalue()
        txt_in_rtf_doc = unicode(txt_in_rtf_doc,codec,'ignore')
        return txt_in_rtf_doc
                        
def convert_ppt_doc_to_txt(src_path, codec, file_ext):
    '''  
    Converts ppt/pptx documents into text and stores them into the file
    mentioned in the destination path.
        
    Arguments: 
                src_path - path to the doc/doc file
                file_ext - codec (Eg: utf-8, utf-16 etc.)
                file_ext - .ppt, .pptx 
    
    Returns: 
                text / text_runs - unicode string
                
    Note: ppt to text conversion requires MS Powerpoint installed in the system.
    '''
    if file_ext == '.ppt':
        try:
            app = win32com.client.DispatchEx("Powerpoint.Application")
            pptfile = app.Presentations.Open(src_path, ReadOnly=0, Untitled=0,
                                              WithWindow=0)
            slides = pptfile.Slides
            text = ''
            for slide in slides:
                for item in slide.Shapes:
                    if item.HasTextFrame:
                        textRange = item.TextFrame.TextRange
                        text += ' ' + textRange.Text
            # changed encoding from utf-8 to ascii 
            text = text.decode('ascii', 'ignore')
        except Exception ,e:
            print 'Error. Cannot find Microsoft Powerpoint Application to convert.'+str(e)
            text = str(e)
            return text
    
    elif file_ext == '.pptx':     
        prs = Presentation(src_path)
        text_runs = ''      
        for slide in prs.slides:
            for shape in slide.shapes:
                if not shape.has_textframe:
                    continue
                for paragraph in shape.textframe.paragraphs:
                    for run in paragraph.runs:
                        text_runs+=' ' + run.text
        text_runs = text_runs.encode(codec,'ignore')
        text_runs = unicode(text_runs,codec,'ignore')
        return  text_runs        
        
'''
-----------------------------------------------------------------------------
Main function

''' 

if __name__ == '__main__':

    arg_parser = argparse.ArgumentParser('''

    Converts popular document formats like pdf, doc, docx, xls, xlsx, ppt, 
    pptx, and rtf into plain text 

    Example: 
        python TextExtractor -h
        python TextExtractor -d "abc.pdf" -o "abc.txt" 

    ''')
    arg_parser.add_argument("-d", dest="doc_path", type=str, 
                            help="Document path", required=True)
    arg_parser.add_argument("-o", dest="text_doc_path", type=str, 
                            help="Output document path", required=True)
    arg_parser.add_argument("-c", dest="codec", type=str, help="Codec", 
                            default="UTF-8")
    args = arg_parser.parse_args()

    if not os.path.exists(args.doc_path):
        print 'Please enter a valid document path.'
        sys.exit(-1)
        
    try: 
        with open(args.text_doc_path, 'w') as fw: 
            ftc = TextExtractor()
            doc_text = ftc.identify_and_convert(args.doc_path, args.codec)
            print >>fw, doc_text 
    except Exception as e: 
        print "Execption: ", e
    
    